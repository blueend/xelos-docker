# Configuration with ENV/SECRET vars in docker environment

You can use environment variables to set/change the configuration of your XELOS installation.
Many vars can also be passed via secrets mount (unless its a container setting). 
Please make sure that XELOS_CONFIG_ENV_UPDATE is activated, this will automatically create a config.env.json file on container startup.


## Container Settings

- TZ: 'Europe/Berlin'
- NGINX_ENFORCE_SSL: <1|0> (Default: 1, Set to 0 to allow for HTTPS proxy and accept connections on port 80)
- NGINX_DISABLE_IPV6: <1|0> (Default: 0, Should NGINX listen only on ipv4?)
- NGINX_SERVER_NAME: master.intranet.example.com
- PHP_FPM_MAX_CHILDREN: <auto|4|8|..> ("auto" will set children based on memory estimation)
- PHP_FPM_MIN_SPARE: 2
- PHP_FPM_MAX_SPARE: 4

- XELOS_ENVIRONMENT: <production|development> (default: production)
- XELOS_AUTO_UPDATE: <1|0> (Triggers DB migration and XELOS Update checks on container startup)
- XELOS_ENABLE_CRONJOBS: <1|0> (Please activate cronjobs on master node only)
- XELOS_CONFIG_ENV_UPDATE: <1|0> (Default:0, Set to 1 to create config.env.json on startup)

- XELOS_AUTO_INSTALL: <installCode|0> (Default: 0, If installCode is provided and no installation directory is detected yet, the container will attempt to download and seed XELOS automatically)
- XELOS_LICENSE_KEY: "asdc-asdc-ascd-.." (Optional: Set License Key)
- XELOS_LICENSE_TOKEN: <AccessToken> (Optional: Set License Token)
- XELOS_WAIT_FOR_DB: <1|0> (Default: 1, Container will wait until DB is available. Check requires XELOS_DB_HOST)
- XELOS_DB_HOST: <ip/host> (Required to use wait_for_db and 
- XELOS_ADMIN_PASS: "<initialPassword>" (Can be used to reset password on startup)
- XELOS_ADMIN_USERID: <id>

## DB Settings

- XELOS_DB_SERVER: <ip/host>
- XELOS_DB_DATABASE: <dbname:xelos>
- XELOS_DB_USER: "<DBUser>"
- XELOS_DB_PASS: "<DBPassword>"

Port is currently fixed to port 3306

## REDIS Settings

- XELOS_CACHE_SERVER: <ip/host:port,ip2/host2:port2,...> (Syntax for type: rediscluster) 
- XELOS_CACHE_SERVER: <ip/host> (Syntax for type: redis, port fixed to 6379)
- XELOS_CACHE_AUTH: "<CacheAuthKey>"
- XELOS_CACHE_TYPE: <redis|rediscluster>
- XELOS_SESSION_STORAGE: <redis|rediscluster>
- XELOS_SESSION_REDIS_SERVER_PORT: <6379> (Only for type: redis)
- New: XELOS_REDIS_URL: ""
- New: XELOS_REDIS_PASSWORD: ""


## ELASTIC Settings

- XELOS_FORCE_SEARCH_ES_HOST: "[\"http://app1:9200\",\"http://app2:9200\",\"http://app3:9200\"]"
- XELOS_FORCE_SEARCH_ES_USER: "elastic"
- XELOS_FORCE_SEARCH_ES_PASSWORD: "<ElasticSearchPassword>"
- New: XELOS_ELASTIC_URL: "http://elastic1:9200|http://elastic2:9200"
- New: XELOS_ELASTIC_USER: ""
- New: XELOS_ELASTIC_PASSWORD: ""


## LDAP Directory Sync

- XELOS_LDAP_OPT_SIZELIMIT: 1000 (Maximum number of entries that can be returned from a ldap search)
- XELOS_LDAP_PAGEDSEARCH: false ( true = Use paged controls instead of deepsearch | experimental) 
- XELOS_LDAP_DEEPSEARCH_MIN_LENGTH: 0 ( 0 = Server allows for '*' search, 1 = Server requires at least 'a*', 2 = 'aa*')
- XELOS_LDAP_DEEPSEARCH_CHARS: "" (Allows for additional deepsearch chars, XELOS will deepsearch [0-9],[a-z],[äöü] by default)

*Please note that the following options are only available via the config.custom.php file:*

- LDAP_OPT_X_TLS_CERTFILE_PATH (Sets the full-path of the certificate file)
- LDAP_OPT_X_TLS_KEYFILE_PATH (Sets the full-path of the certificate key file)


## AV Settings

- XELOS_SI_CLAMSCAN: "clamdscan --fdpass"

## Optional Monitoring Settings

- XELOS_LOG_GELF_URL: "udp://graylogserver:12201"
- XELOS_LOG_GLOBAL_CONTEXT: "{\"var\":\"value\"}"

## XELOS Settings

Excerpt of important XELOS configuration options

- XELOS_PASSWORD_SALT: "<PasswordSalt>"
- XELOS_FORCE_mail_disable: 1
- XELOS_COOKIE_DOMAIN: ".intranet.example.com"
- XELOS_DISABLE_UPDATE: 1 (Disable update UI)
- XELOS_MAINTENANCE_FILE: "<path/to/system_unusable>"
- XELOS_EMAIL_SENDER: "intranet-support@example.com"
- XELOS_LOG_EMAIL: "serveradmin@blueend.com"
- XELOS_PROTOCOL: "https://"
- XELOS_PHP_MEM_LIMIT: "256M"
- XELOS_NODE_NAME: "<ServerName>"
- XELOS_SECRETS_LOCATION: "<path/to/secrets>" (e.g. /run/secrets/xelos_secrets)
- XELOS_NODE_CRON: 1
