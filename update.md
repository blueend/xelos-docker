# Upgrading from XELOS 10 container environment #

This upgrade guide assumes you are running a XELOS 10 environment:

- mysql 8.x 
- elasticsearch 8.x 
- redis 7


Preparation
---------

1. Make a snapshot (slim) : `php xf snapshot:take`
2. Update to the latest X10 packages
3. Continue with the next steps


Meeting the XELOS 11 Upgrade Wizard requirements
----------------------

1. Update your container services to meet the required versions:

**Stop your running containers**

`docker compose stop`

**Modify your docker-compose.yml (or swarm/kubernetes setup):**

We are upgrading the web container to XELOS 11/PHP 8.2, upgrade elasticsearch and redis to a newer version (no breaking changes) and add the service workers (we are **replacing libreoffice**, **adding video** )

~~~
  web:
    image: xelos/xelos-server:xelos-11
    networks:
      backend:
        # Check for valid domain resolution (worker callback!)
        aliases: ["www.my-domain.com", "intranet.domain.com"]

  # == XELOS Worker: Video Convert Service =============
  video:
    image: xelos/xelos-worker:video
    hostname: video.xelos.local
    healthcheck:
      test: [ "CMD", "curl","-fk", "https://localhost:9501" ]
      timeout: 20s
      retries: 10
    restart: unless-stopped
    networks:
      backend:
    logging: *default-logging

  # == LibreOffice: File Conversion Worker ========
  libreoffice:
    image: xelos/xelos-worker:libreoffice
    hostname: libreoffice.xelos.local
    healthcheck:
      test: [ "CMD", "curl","-fk", "https://localhost:9501" ]
      timeout: 20s
      retries: 10
    restart: unless-stopped
    networks:
      - backend
    logging: *default-logging  
    
~~~


Optional, but recommended (Change redis to alpine for better security rating):

~~~
  redis:
    image: redis:7-alpine
~~~

Update services:

~~~
  elastic:
    image: docker.elastic.co/elasticsearch/elasticsearch:8.13.4
~~~


Update the docker images:

`docker compose pull`


Restart all services as usual:

`docker compose up -d`

Continue with the XELOS 11 upgrade wizard...