## **You can find upgrade instructions for your XELOS 10 environment [here](https://bitbucket.org/blueend/xelos-docker/src/master/update.md)**

# XELOS 11 Docker for Production #

This repository contains the setup to get XELOS up and running in docker containers.

## What is XELOS?

XELOS Social Workplace is an enterprise grade **Social Intranet Software** which supports the communication and collaboration aspects of modern companies. [More information about XELOS and its features can be found on the XELOS Social Intranet website.](https://xelos.net) 

**Manuals** (User, Administration, Developer) can be found on our customer portal at [my.xelos.net](https://my.xelos.net). 

To run XELOS you also need an **install package** and a corresponding **license**. Please contact your partner or the XELOS sales team for more details.

[If you are interested in testing the software you can also request a free account or trial setup.](https://xelos.net/de/testen/)


# Quick Setup / Overview #

1. Prepare environment / prerequisites
2. Create initial directory structure
3. Configure `docker-compose.yml`
  - Adjust NGINX hostname
  - Change passwords
  - Enable/Disable Secondary Services such as Elastic Search, Office Files Preview, Video Files Conversion, ClamAV  
4. Prepare service containers
5. Start Services (`docker compose up`)

## 1. Preparing your environment ##

You need a docker enabled operating system for running linux x86_64/amd64 containers, i.e. linux with docker services installed.  

We recommend using a CentOS based distribution (e.g. AlmaLinux), we discourage the production use of a MS Windows or MacOS based systems.


### Installing Docker Services and Docker-Compose ###

Please follow the [official documentation](https://docs.docker.com/engine/install/ "Install Docker Engine") on how to install docker services for your distribution.


### Specs ###

It is **recommended having 8 CPU Cores and 16GB RAM** to host a full XELOS instance. 

Depending on the expected number of online users and intended application usage this recommendation may vary.



## 2. Create initial directory structure ##

Clone this repository / Download Release File to create initial directory structure.

Please [download](https://bitbucket.org/blueend/xelos-docker/downloads/) or clone this repository to your server environment:


    mkdir -p /server/xelos-intranet/
    cd /server/xelos-intranet
    git clone https://bitbucket.org/blueend/xelos-docker.git .


Example above assumes installing to directory `/server/xelos-intranet`. This location can be changed - the following steps are considered relative to this location.



## 3. Configure `docker-compose.yml` ##

Copy `docker-compose.dist.yml` to `docker-compose.yml` to start configuring your docker containers.

    cp ./docker-compose.dist.yml docker-compose.yml

The most important values that need adjusting are:

~~~
docker-compose.yml

services:
  web:
    environment:
      XELOS_DB_PASS: <YourDBSecret>
      XELOS_ADMIN_PASS: <InitialAdminPassword>
      XELOS_SYSTEM_NAME: <My Intranet>
      XELOS_AUTO_INSTALL: <40 Char InstallCode>
      NGINX_SERVER_NAME: <xelos.example.com>
      NGINX_ENFORCE_SSL: <1=local SSL termination | 0=reverse proxy ssl - see SSL section below>
      

  db:
    environment:
      MYSQL_ROOT_PASSWORD: <YourDBSecret>
      MYSQL_PASSWORD: <YourDBSecret>
  
~~~

- To enable the automatic installation process you should enter your installation code as XELOS_AUTO_INSTALL param
- You can also check wether you require all services or want to disable some before first startup


## 4. Prepare service containers ##

Some containers require additional preparation. Please make sure to follow these preparations carefully otherwise you might experience problems running the containers properly.

### Data Directory ###

To avoid losing relevant data, each container will save their data into the ./data/ directory using the paths/mounts defined in the docker-compose.yml. 

Each container needs read/write access to their respective data directory. Please make sure that the following directories are created and ownership is correct:

    mkdir ./data/elasticsearch/
    chown 1000:1000 ./data/elasticsearch/
    
    mkdir ./data/mysql/
    chown 1001:1001 ./data/mysql/
    
    mkdir ./data/redis/
    chown 999:999 ./data/redis/



### Elastic Search: vm.max_map_count ###

To successfully run the elastic search container, please make sure that you are increasing the virtual memory map count by adding the following line in your `/etc/sysctl.conf` :

    vm.max_map_count=262144
  
Apply settings by running `sysctl -p` afterwards.
	


### Redis: vm.overcommit_memory ###

To avoid warning messages when running REDIS you should to DISABLE THP Support for your Kernel and enable overcommit_memory:

    echo never > /sys/kernel/mm/transparent_hugepage/enabled
    
For instructions on how to permanently disable THP see this [RHEL article](https://access.redhat.com/solutions/46111).    
    
To enable overcommit you can add the following line to your `/etc/sysctl.conf`:

    vm.overcommit_memory = 1

Apply sysctl settings by running `sysctl -p` afterwards.


## 5. Start services ##

1. Start all services with `docker compose up`, you should be able to see potential errors as logs are now directed to console output
2. If startup was successful, quit the running process with STRG+C
3. Remove ADMIN Password, Disable AUTO_INSTALL in `docker-compose.yml`
4. Run `docker compose up -d` to start services in background (regular startup)


# Documentation / Configuration of Docker Container #

![Docker Setup](./docs/XELOS_Docker_Setup.png)

The setup currently consists of the following containers serving different purposes:

 - **web** - Contains the XELOS Server (Frontend and Backend) and handles all external requests
 - **db**  - The mysql database
 - **redis** - A redis server for caching of the frontend/backend services
 - **clamav** - ClamAV antivirus daemon for checking file uploads
 - **elastic** - ElasticSearch search backend for content indexing
 - **libreoffice** - LibreOffice Worker Service for document conversion and preview generation
 - **video** - Video Worker Service for video conversion and preview generation
 - **log-rotate** - Maintenance helper for log rotation
 
You should keep the service names as in the default setup as some endpoints are already preconfigured and renaming may cause issues due to changes in the internal endpoints.
    
If you are migrating from a different setup, you may need to make some adjustments to your existing configuration to match the services:
    
**/system/config/config.custom.php**

    #Use REDIS for Caching
    define('XF_CACHE_TYPE', 'redis');
    define('XF_CACHE_SERVER', 'redis');

    #Use REDIS for Session (optional)
    define('XF_SESSION_STORAGE', 'redis');

    #Use CLAMD Scan
    define('XF_SI_CLAMSCAN', 'clamdscan');  
    
    
**Search Configuration**

1. Set **Search Engine** to **elasticsearch**
2. Set **Host** to **http://elastic:9200**

**System Worker Configuration**

1. **LibreOffice Worker** Default URI: **https://libreoffice:9501**
2. **Video Worker** Default URI:  **https://video:9501**

More information about XELOS Service Workers can be found at https://hub.docker.com/r/xelos/xelos-worker

## S6 / Root / Non-Root Container ##

The container is using the [S6 overlay](https://github.com/just-containers/s6-overlay) and combines NGINX with PHP-FPM to servce the application. 
The NGINX and PHP-FPM configuration and processes are tuned to work together: NGINX as external process for HTTP communication and PHP-FPM to run the application stack. 
Theoretically the application is primarily run by the PHP-FPM process, but is not accessible by clients or secure when served over the FPM/FastCGI interface, while serving data over HTTP/HTTPs via NGINX is common practice and can easily be secured.
Therefore the PHP process is run internally, while NGINX is exposing ports and securing the communication.

The default XELOS container is starting up as root and will internally start the primary processes as uid:48 (php-fpm) and uid:100 (nginx).
Using root allows for inplace software upgrades, easier backup/restore via the CLI administration and permission corrections. 
Running 2 different users also allows for internal seperation, i.e. the web process is not allowed to modify PHP files and can be furter limited in file access, while the update and backup process run as root is capable of applying hotfixes.

Running the root image is recommended for docker-compose installations on single servers. The default exposed ports are http:80/https:443.

When running kubernetes or swarm installations it is recommended to use the non-root variant as updates/hotfixes are usually applied by providing new containers. 
The non-root variant is **running php-fpm and nginx as uid:10000**. The default exposed **ports are http:8080/https:8443**.

**Switching from Root to Non-Root**

 - Volumes with logs and data should be writable by uid:10000 (stop container, chown volumes, switch to non-root container, start new container)
 - Change default ports exposed or used in upstream proxies (e.g. caddy upstreams 8080)
 - Update configuration (config.custom.php) or env params, if present:
 
```
define('XF_SYSTEM_UID','xelos');
define('XF_WEBSERVER_UID','xelos');
```


## Adding SSL certificate ##

It is required to access XELOS via https://. 

- You can deploy XELOS behind a reverse proxy or firewall which is serving your SSL certificate.
- You can upload/change the certificate used by the docker setup
- You can use a reverse proxy container to automatically obtain certificates via lets encrypt. 


### Include Custom SSL Certificate ###

1. Make sure SSL volume is mounted in docker-compose.yml e.g. *./ssl/certs:/server/certs*
2. Replace the default SSL certificate in the ssl/certs/ folder with your custom SSL certificate


### Using Caddy for LetsEncrypt SSL ###

The supplied **docker-compose.dist.yml** contains a simple example to integrate caddy as part of the XELOS service stack. Please make sure your firewall settings support the communication for the LetsEncrypt process.

1. Include the service definition of caddy ( lucaslorentz/caddy-docker-proxy:ci-alpine )
2. Adapt the web container definition (NGINX_ENFORCE_SSL: 0, disable port binding and communicate your domain as label to the caddy service : 

```
  web:
   ...
    environment:
      ...
      NGINX_ENFORCE_SSL: 0

    #ports:
    #  - "80:80"           # WEB (NON-SSL)
    #  - "443:443"         # WEB (SSL)

    labels:
      caddy: "xelos.example.com"
      caddy.reverse_proxy: "{{upstreams 80}}"

```

3. Shutdown the containers: `docker-compose down` 
4. Restart the services: `docker-compose up -d`

### Using Traefik for LetsEncrypt SSL ###

Traefik can be used as a reverse proxy and terminates ssl with lets encrypt certificates. 

We recommended running Traefik with a separate docker-compose.yml and create a different backend network to link to.

Please also check the [official documentation of Traefik](https://docs.traefik.io/).


 
## Monitoring and Maintenance ##

Primary logging of all docker containers will happen in journalctl, you can easily filter the respective logs by adding the param "container_name":
   
    journalctl CONTAINER_NAME=xelos_web_1
   
Please add a logrotate configuration on your hostsystem ( /etc/logrotate.d/xelos_docker ) to rotate nginx and php-fpm logs for this setup, e.g.
   
    <path>/log/*/*.log {
      daily
      copytruncate
      missingok
      notifempty
      create 0666 root root
      rotate 30
      compress
      delaycompress
    }
   
You can use your standard process monitoring to check your setup. Please make sure you have **at least** the following processes running: nginx, php-fpm and mysqld 

To download and apply container updates please run:
   
1. `docker compose pull`
2. `docker compose up -d`
   
For specific upgrades (e.g. upgrading from XELOS 7 to 8 or PHP 7.0 to PHP 7.1): Please follow the update guides published on my.xelos.net. 
In case of any changes to your docker-compose.yml always make sure to use `docker-compose up -d` afterwards to apply your changes to the running containers and for future restarts.



## Troubleshooting ##
To reset the docker setup to the inital state you need to remove all data. 
To do so, please:

1. Shutdown and remove all running containers with docker compose down --remove-orphans
2. Remove all data in the /data folder
3. Remove the config.custom.php from the xelos config folder
4. Startup with docker compose up

### IP / Routing Conflict ###
If not configured otherwise docker will setup a docker0 bridge using 172.17.0.0 as default network when starting the service. 
Starting the XELOS container without modifications will create a custom bridge on 172.18.0.0.
If this conflicts with routing to your internal network you may need to change this behaviour.

Create /etc/docker/daemon.json if it does not exist and use the settings to configure the default docker0 bridge. 

**Example** (more options can be found on [docs.docker.com](https://docs.docker.com/network/)):

    {
      "bip": "192.168.1.5/24",
      "fixed-cidr": "192.168.1.5/25",
      "ipv6": false
    }

Reload the docker configuration file:

     systemctl reload docker
    
Use the commented options in the [docker-compose.yml](https://bitbucket.org/blueend/xelos-docker/src/master/docker-compose.dist.yml) to configure ip/bridge of the XELOS containers.


### Elastic Search Password Authentication ###

To enable basic authentication, change `xpack.security.enabled=false` to `xpack.security.enabled=true` in your `docker-compose.yml`. 
The initial password can be set with the enviroment variable `ELASTIC_PASSWORD` for the user `elastic`.

**It is recommended to enable authentication in production for security reasons!** 

To change the password of an existing Elastic Search node follow these steps:

- Enable Security with **xpack.security.enabled=true** ENV (see above)
- Stop, remove and restart elastic container (keeps the index as this is mounted)
 - `docker compose stop elastic`
 - `docker compose rm elastic`
 - `docker compose up -d elastic`
- Start bash in elastic container
 - `docker-compose exec elastic bash` 
- Run elastic setup CLI
 - `bin/elasticsearch-setup-passwords auto -u "http://localhost:9200"`
- Enter elastic user + password in Search Helper Configuration via XELOS Systemadmin